<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tasks;
use Redirect;
use Illuminate\Support\Facades\Input;

class TasksController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index()
    {
    	$tasks = Tasks::orderBy('id')->get();
		return view('tasks.index',compact('tasks'));

    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
    public function create()
    {
    	return view('tasks.create');
    }

    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$tasks = Tasks::create(Input::all());
		return Redirect::route('tasks.index');
	}

}
