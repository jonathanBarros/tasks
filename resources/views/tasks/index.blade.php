<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Lista de Tarefas</title>
</head>
<body>
  <ul>
  <?php foreach ($tasks as $tarefa): ?>
    <li>
        Id: <?php echo $tarefa->id ?>
        <br/>
        Título: <?php echo $tarefa->title ?>
        <br/>
        Corpo: <?php echo $tarefa->body ?>
    </li>
  <?php endforeach ?>
  </ul>
  <a href="{!!URL::route('tasks.create')!!}">Cadastro</a>
</body>
</html>